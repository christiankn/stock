package code.stocks;

import java.util.Date;

public class Table {
    private Date date;
    private double close;

    public Table(Date date, double close) {
        this.date = date;
        this.close = close;
    }

    public Date getDate() {
        return date;
    }
    public double getClose() {
        return close;
    }
}
