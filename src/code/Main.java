package code;

import code.GUI.Container;
import code.game.Player;
import code.game.StockGame;
import code.stocks.Table;
import code.strategy.BuyOnceSellLastCall;
import code.strategy.RandomStrategy;
import code.services.StockService;
import code.strategy.RandomStrategyV2;
import code.strategy.Strategy;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
/*
    TODO
    3: Incorperate the API key in this bitch in its own jave file that is not added to git
    4: More stocks and more Finance theory
    5: More info in table
    6: Buttons to stop go back and forth
    7: lav close list i en forløkke tidligere (allerede når dataen hentes og den table list laves)
    8: We start with round 40, should this be changed?
*/
public class Main {
    private static ArrayList<Table> tableList = new ArrayList<>();
    private static ArrayList<Double> closeList = new ArrayList<>();
    private static HashMap<Strategy, Player> playerList = new HashMap<>();

    public static void main(String[] args) {
        run();
    }

    public static void run() {
        initiatePlayers();
        Setup();
        Container container = new Container(playerList,closeList);
        StockGame game = new StockGame();
        System.out.println("Rounds: " + tableList.size());
        int rounds = tableList.size();

        //loop for running the game and repainting everything
        for (int i = 0; i+40 < rounds; i++) {

            //in the game I work with i+40 because it is the easiest thing to do with regards to the graph
            //run a game round
            Table table = tableList.get(i+40);
            Iterator it = playerList.entrySet().iterator();
            while (it.hasNext()) {
                HashMap.Entry pair = (HashMap.Entry)it.next();
                Strategy strategy = (Strategy) pair.getKey();
                Player player = (Player) pair.getValue();
                if(i+40 >= rounds-1){
                    game.sell(table.getClose(),player);
                }else{
                    game.round(player, strategy, table);
                }
            }
            //repaint graphPanel and infoPanel
            ArrayList<Double> subList;
            if (closeList.size() > 40) {
                List<Double> list = closeList.subList(i, i + 40);
                subList = new ArrayList<>(list);
                container.repaint(subList, playerList);
            }

            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public static void initiatePlayers(){
        RandomStrategy rStrat = new RandomStrategy();
        RandomStrategyV2 rStratV2 = new RandomStrategyV2();
        BuyOnceSellLastCall buyStrat = new BuyOnceSellLastCall();
        playerList.put(rStrat, new Player());
        playerList.put(rStratV2, new Player());
        playerList.put(buyStrat, new Player());
    }


    public static void noInternetSetup() {
        String dateString = "92-11-11";
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Random r = new Random();
        try {
            Date date = format.parse(dateString);
            for (int i = 0; i < 200; i++) {
                Double close = r.nextDouble();
                Table table = new Table(date, close);
                tableList.add(table);
                closeList.add(close);
            }
        } catch (Exception e) {
        }
    }

    public static void testCall() {
        Setup();
        System.out.println("Simple Moving Average: " + smaTen());
    }

    public static void Setup() {
        StockService stockService = new StockService();
        stockService.getStocks();
        tableList = stockService.getTableList();
        for (int i = 0; i < tableList.size(); i++) {
            closeList.add(tableList.get(i).getClose());
        }
    }

    public static double smaTen() {
        //simple moving average
        double sum = 0;
        for (int i = tableList.size() - 10; i < tableList.size(); i++) {
            Table table = tableList.get(i);
            sum += table.getClose();
        }
        double res = sum / 10;
        return res;
    }
}
