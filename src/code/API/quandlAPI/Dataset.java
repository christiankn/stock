package code.API.quandlAPI;

import java.util.ArrayList;
import java.util.List;

public class Dataset {

    private int id;
    private ArrayList<List<String>> data = new ArrayList<List<String>>();

    public ArrayList<List<String>> getData() {
        return data;
    }

    public void setData(ArrayList<List<String>> data) {
        this.data = data;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}




