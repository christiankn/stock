package code.API.quandlAPI;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface QuandlAPI {

    @GET("datasets/WIKI/{stock}")
    Call<HistorialData> call(@Path("stock") String stock,
                             @Query("api_key") String QuandlAPI_KEY);

    @GET("datasets/WIKI/{stock}")
    Call<HistorialData> call(@Path("stock") String stock,
                                       @Query("start_date") String startDate,
                                       @Query("end_date") String endDate);

}
