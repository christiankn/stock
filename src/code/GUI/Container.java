package code.GUI;

import code.game.Player;
import code.strategy.Strategy;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.HashMap;

public class Container extends JFrame {
    GraphPanel graphPanel;
    InfoPanel infoPanel;
    HashMap<Strategy, Player> playerList;

    public Container(HashMap<Strategy, Player> playerList, ArrayList<Double> list) {
        this.playerList = playerList;
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setCompoments(list);
        setTitle("Stock");
        pack();
        setLocationRelativeTo(null);
        setVisible(true);
        createMenu();
    }

    public void setCompoments(ArrayList<Double> list) {
        graphPanel = new GraphPanel(list);
        graphPanel.setPreferredSize(new Dimension(900, 600));

        infoPanel = new InfoPanel(playerList);
        infoPanel.setBackground(Color.RED);

        infoPanel.setLayout(new BoxLayout(infoPanel, BoxLayout.Y_AXIS));
        infoPanel.setPreferredSize(new Dimension(500, 600));

        add(graphPanel, BorderLayout.CENTER);
        add(infoPanel, BorderLayout.EAST);
    }

    public void createMenu() {
        MenuBar menuBar = new MenuBar();
        Menu menu = new Menu("Menu");

        MenuItem addStrat = new MenuItem("Add Strategy");
        MenuItem quit = new MenuItem("Quit");

        menu.add(addStrat);
        menu.add(quit);
        menuBar.add(menu);
        setMenuBar(menuBar);
    }

    public void repaint(ArrayList<Double> list, HashMap<Strategy, Player> playerList){
        graphPanel.setScores(list);
        infoPanel.resetPlayers(playerList);

    }
}
