package code.GUI;

import code.game.Player;
import code.strategy.Strategy;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;


public class InfoPanel extends JPanel{
    HashMap<Strategy, Player> playerList;
    ArrayList<JLabel> jLabelList = new ArrayList<>();

    public InfoPanel(HashMap<Strategy, Player> playerList){
        this.playerList = playerList;
        setPlayerList();

    }

    public void setPlayerList(){
        Iterator it = playerList.entrySet().iterator();
        while (it.hasNext()) {
            HashMap.Entry pair = (HashMap.Entry)it.next();
            Strategy strategy = (Strategy) pair.getKey();
            Player player = (Player) pair.getValue();
            String labelText = strategy.getClass().getSimpleName() + ": " + player.getStash();
            JLabel jLabel = new JLabel(labelText);
            jLabelList.add(jLabel);
            this.add(jLabel);
        }
    }

    @Override
    public void paintComponent(final Graphics g) {
        super.paintComponent(g);
        Iterator it = playerList.entrySet().iterator();
        int i = 0;
        while (it.hasNext()) {
            HashMap.Entry pair = (HashMap.Entry)it.next();
            Strategy strategy = (Strategy) pair.getKey();
            Player player = (Player) pair.getValue();
            String labelText = strategy.getClass().getSimpleName() + ": " + player.getStash();
            jLabelList.get(i).setText(labelText);
            i++;
        }
    }
    public void resetPlayers(HashMap<Strategy, Player> playerList) {
        this.playerList = playerList;
        validate();
        this.repaint();
    }
}
