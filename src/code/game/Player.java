package code.game;

public class Player {
    private boolean haveStock = false;
    private double stash = 0;

    public void setStash(double stash) {
        this.stash = stash;
    }

    public double getStash() {
        return stash;
    }

    public boolean haveStock() {
        return haveStock;
    }


    public void setStock(boolean haveStock) {
        this.haveStock = haveStock;
    }

    public String toString(){
        return "Player Stash: " + getStash();
    }
}
