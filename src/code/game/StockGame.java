package code.game;

import code.stocks.Table;
import code.strategy.Strategy;

import java.util.ArrayList;

public class StockGame {
    private static ArrayList<Table> tableList = new ArrayList<>();

    public void round(Player player, Strategy strategy, Table table) {
        double close = table.getClose();
        if (strategy.run(table, player)) {
                buy(close, player);
        } else {
                sell(close, player);
        }
    }

    public void buy(double close, Player player) {
        if (!player.haveStock()) {
            double stash = player.getStash();
            player.setStock(true);
            player.setStash(stash - close);
        } else {
            System.out.println("You cannot buy because you can only have one stock");
        }
    }

    public void sell(double close, Player player) {
        if (player.haveStock()) {
            double stash = player.getStash();
            player.setStock(false);
            player.setStash(stash + close);
        } else {
            System.out.println("You can only sell when you have a stock");
        }
    }
}


