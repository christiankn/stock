package code.strategy;

import code.game.Player;
import code.stocks.Table;

import java.util.Random;

public class RandomStrategy implements Strategy
{
    @Override
    public boolean run(Table table, Player player) {
        if(player.haveStock()){
            Random r = new Random();
            return r.nextBoolean();
        }else {
            return true;
        }
    }
}
