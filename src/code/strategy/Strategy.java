package code.strategy;

import code.game.Player;
import code.stocks.Table;
public interface Strategy {
    public boolean run(Table table, Player player);
}
