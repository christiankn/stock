package code.services;

import code.API.quandlAPI.HistorialData;
import code.API.quandlAPI.QuandlAPI;
import code.API_KEY;
import code.RetrofitRequestMaker;
import code.stocks.Table;
import retrofit2.Call;
import retrofit2.Retrofit;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class StockService {
    private static ArrayList<Table> tableList = new ArrayList<>();

    public void getStocks() {
        String baseUrl = "quandl.com";
        int port = 443;
        String pathSegment = "api/v3/";
        String fb = "FB.json";
        String API_KEY = code.API_KEY.getKey();
        Retrofit retrofitRequest = new RetrofitRequestMaker(baseUrl, port, pathSegment).getRetrofitRequest();
        QuandlAPI quandlAPI = retrofitRequest.create(QuandlAPI.class);
        Call<HistorialData> call = quandlAPI.call(fb, API_KEY);
        //asynchronous call
        try {
            HistorialData historialData = call.execute().body();
            structureData(historialData);
        } catch (Exception e) {
            System.out.println("Something went wrong in StockService");
        }
        System.out.println(call.request().toString());
    }

    public static void structureData(HistorialData historialData) throws ParseException {
        ArrayList<List<String>> data = historialData.getDataset().getData();
        for (List list : data) {
            String dateString = list.get(0).toString();
            String closeString = list.get(4).toString();
            DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            Date date = format.parse(dateString);
            double close = Double.parseDouble(closeString);
            Table table = new Table(date, close);
            tableList.add(table);
        }
    }

    public ArrayList<Table> getTableList() {
        Collections.reverse(tableList);
        return tableList;
    }

//    // A working example of the API and service combined in one to give an overview of that it all actually does.
//    public void getStocksCsv() {
//        String baseUrl = "quandl.com";
//        String pathSegment = "api/v3/";
//        String fb = "FB.json";
//        System.out.println("hej");
//
//        HttpUrl httpUrl = new HttpUrl.Builder()
//                .scheme("https")
//                .port(443)
//                .host(baseUrl)
//                .addEncodedPathSegments(pathSegment)
//                .build();
//
//        Retrofit retrofit = new Retrofit.Builder()
//                .baseUrl(httpUrl)
//                .addConverterFactory(GsonConverterFactory.create())
//                .build();
//
//        QuandlAPI restService = retrofit.create(QuandlAPI.class);
//        Call<HistorialData> call = restService.call(fb);
//        System.out.println(call.request().toString());
//        //asynchronous call
//        try {
//            HistorialData historialData = call.execute().body();
//            structureData(historialData);
//        } catch (Exception e) {
//        }
//    }
}
