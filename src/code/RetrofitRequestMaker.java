package code;

import okhttp3.HttpUrl;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitRequestMaker {
    private Retrofit retrofitRequest;

    public RetrofitRequestMaker(String baseUrl, int port, String pathSegment) {
        HttpUrl httpUrl = new HttpUrl.Builder()
                .scheme("https")
                .port(port)
                .host(baseUrl)
                .addEncodedPathSegments(pathSegment)
                .build();

        retrofitRequest = new Retrofit.Builder()
                .baseUrl(httpUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    public Retrofit getRetrofitRequest() {
        return retrofitRequest;
    }
}